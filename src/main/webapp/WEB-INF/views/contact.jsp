<%@ taglib uri="http://www.springframework.org/tags/form"
    prefix="springForm"%>
<html>
<head>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.6.1.min.js"></script>

<script type="text/javascript" src="resources/js/menu.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/menu.css">
</head>
<body>
	<ul id="menu_wrap" class="Blue">
		<li class="button"><a href="home">Home</a></li>
		<li class="button"><a href="contacts">Contacts</a></li>
		<li class="button"><a href="#">Events</a></li>
		<li class="button"><a href="#">Follow Ups</a></li>
		<li class="button"><a href="#">Notes</a></li>
		<li class="button"><a href="#">Email</a></li>
		<li class="button"><a href="#">About us</a></li>
		<li class="search"><a href="#"></a><input type="text"
			placeHolder="Search" /></li>

	</ul>
</body>
<springForm:form method="POST" commandName="contact"
        action="contacts">
        <table>
            <tr>
                <td>First Name:</td>
                <td><springForm:input path="firstName" /></td>
                <td><springForm:errors path="firstName" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><springForm:input path="lastName" /></td>
                <td><springForm:errors path="lastName" cssClass="error" /></td>
            </tr>
            
            <tr>
                <td colspan="3"><input type="submit" value="Save Customer"></td>
            </tr>
         </table> 
    </springForm:form>
</html>


