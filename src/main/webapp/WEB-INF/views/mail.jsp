<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mail</title>
<jsp:include page="Includes/head.jsp" />
</head>
<body>
	<div id="page-container"
		class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
		<!-- Side Overlay-->
		<jsp:include page="Includes/header.jsp" />
		<!-- END Side Overlay -->

		<!-- Sidebar -->
		<jsp:include page="Includes/Left-Side-Menu-Content.jsp" />
		<!-- END Sidebar -->

		<!-- Header -->
		<!-- header and right side overlay is both in header.jsp -->
		<!-- END Header -->

		<!-- Main Container -->
		<div id="main-container">
		 <c:url var="addAction"
			value="/template/add"></c:url>
		<h1>Templates</h1>


		<form:form action="${addAction}" commandName="template">
			<table>
				<c:if test="${!empty template.mailTemplateName}">
					<tr>
						<td><form:label path="mailTemplateID">
								<spring:message text="mailTemplateID" />
							</form:label></td>
						<td><form:input path="mailTemplateID" readonly="true"
								disabled="true" /> <form:hidden path="mailTemplateID" /></td>
					</tr>
				</c:if>

				<tr>
					<td><form:label path="mailTemplateName">
							<spring:message text="Template  Name :" />
						</form:label></td>
					<td><form:input path="mailTemplateName" /></td>
				</tr>

				<tr>
					<td><form:label path="mailTemplateSubject">
							<spring:message text="Template  Subject :" />
						</form:label></td>
					<td><form:input path="mailTemplateSubject" /></td>
				</tr>
				<tr>
					<td><form:label path="mailImage">
							<spring:message text="Images for Template:" />
						</form:label></td>
					<td><form:input type="file" path="mailImage" /></td>
				</tr>


				<tr>
					<td><form:label path="mailGreetings">
							<spring:message text="Mail  Greetings :" />
						</form:label></td>
					<td><form:textarea row="3" col="100" path="mailGreetings" />
					</td>
				</tr>

				<tr>
					<td><form:label path="mailHeader">
							<spring:message text="Mail  Header" />
						</form:label></td>
					<td><form:textarea row="5" col="100" path="mailHeader" /></td>
				</tr>

				<tr>
					<td><form:label path="mailBody">
							<spring:message text="Mail  Body :" />
						</form:label></td>
					<td><form:textarea path="mailBody" row="10" col="100" /></td>
				</tr>
				<tr>
					<td><form:label path="mailFooter">
							<spring:message text="Mail  Footer :" />
						</form:label></td>
					<td><form:textarea row="5" col="100" path="mailFooter" /></td>
				</tr>

				<tr>
					<td><form:label path="mailSignature">
							<spring:message text="Mail  Signature :" />
						</form:label></td>
					<td><form:textarea row="3" col="100" type="text"
							path="mailSignature" /></td>
				</tr>



				<tr>
					<td colspan="2"><c:if
							test="${!empty template.mailTemplateName}">
							<input type="submit"
								value="<spring:message text="Edit Template"/>" />
						</c:if> <c:if test="${empty template.mailTemplateName}">
							<input type="submit"
								value="<spring:message text="Add Template"/>" />
						</c:if></td>
				</tr>
			</table>
		</form:form> <br>
		<h3>Template List</h3>
		<c:if test="${!empty listTemplates}">
			<table class="tg">
				<tr>
					<th width="80">Template ID</th>
					<th width="120">Template Name</th>
					<th width="80">Template subject</th>
					<th width="120">Template Images</th>
					<th width="80">Template Greetings</th>
					<th width="120">Template Header</th>
					<th width="80">Template Body</th>
					<th width="120">Template Footer</th>
					<th width="80">Template signature</th>
					<th width="60">Edit</th>
					<th width="60">Delete</th>
				</tr>
				<c:forEach items="${listTemplates}" var="template">
					<tr>
						<td>${template.mailTemplateID}</td>
						<td>${template.mailTemplateName}</td>
						<td>${template.mailTemplateSubject}</td>
						<td><img src="${template.mailImage}" /></td>
						<td>${template.mailGreetings}</td>
						<td>${template.mailHeader}</td>
						<td>${template.mailBody}</td>
						<td>${template.mailFooter}</td>
						<td>${template.mailSignature}</td>

						<td><a
							href="<c:url value='/editTemplate/${template.mailTemplateID}' />">Edit</a></td>
						<td><a
							href="<c:url value='/removeTemplate/${template.mailTemplateID}' />">Delete</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:if> 
		</div>
		<!-- END Main Container -->
		<!-- Footer -->
		<jsp:include page="Includes/footer.jsp"></jsp:include>
		<!-- END Footer -->
	</div>
	<!-- END Page Container -->

	<!-- Apps Modal -->
	<!-- Opens from the button in the header -->
	<!-- the code required for this will be in the header jsp -->
	<!-- END Apps Modal -->

	<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
	<jsp:include page="Includes/scripts.jsp" />

</body>
</html>