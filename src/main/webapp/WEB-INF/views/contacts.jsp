<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Contact</title>
</head>
<body>
 <c:url var="addAction" value="/contact/add" ></c:url>
	<h1> Contacts</h1>

	
	<form:form action="${addAction}" commandName="contact">
	<table>
	<c:if test="${!empty contact.contactName}">
    <tr>
        <td>
            <form:label path="contactId">
                <spring:message text="contactId"/>
            </form:label>
        </td>
        <td>
            <form:input path="contactId" readonly="true" size="8"  disabled="true" />
            <form:hidden path="contactId" />
        </td> 
    </tr>
    </c:if>
    
     <tr>
        <td>
            <form:label path="contactName">
                <spring:message text="Contact  Name :"/>
            </form:label>
        </td>
        <td>
            <form:input path="contactName" />
        </td> 
    </tr>
    <tr>
        <td>
            <form:label path="contactType">
                <spring:message text="Contact Type: "/>
            </form:label>
        </td>
        <td>
            <form:select path="contactType" >
            <form:option value="Starting"> Starting </form:option>
            </form:select>
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="contactEmail">
                <spring:message text="Contact Email: "/>
            </form:label>
        </td>
        <td>
            <form:input path="contactEmail" />
        </td>
    </tr>
    
    <tr>
        <td>
            <form:label path="contactNumber">
                <spring:message text="Contact number: "/>
            </form:label>
        </td>
        <td>
            <form:input path="contactNumber" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="skypeId">
                <spring:message text="Skype id: "/>
            </form:label>
        </td>
        <td>
            <form:input path="skypeId" />
        </td>
    </tr>
    
    <tr>
        <td>
            <form:label path="contactNotes">
                <spring:message text="Contact Notes: "/>
            </form:label>
        </td>
        <td>
            <form:input path="contactNotes" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2">
            <c:if test="${!empty contact.contactName}">
                <input type="submit"
                    value="<spring:message text="Edit Contact"/>" />
            </c:if>
            <c:if test="${empty contact.contactName}">
                <input type="submit"
                    value="<spring:message text="Add Contact"/>" />
            </c:if>
        </td>
    </tr>
</table>  
</form:form>
<br>
<h3>Contact List</h3>
<c:if test="${!empty listContacts}">
    <table class="tg">
    <tr>
        <th width="80">Contact ID</th>
        <th width="120">Contact Name</th>
        <th width="120">Contact Email</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <c:forEach items="${listContacts}" var="contact">
        <tr>
            <td>${contact.contactId}</td>
            <td>${contact.contactName}</td>
            <td>${contact.contactEmail}</td>
            <td><a href="<c:url value='/editContact/${contact.contactId}' />" >Edit</a></td>
            <td><a href="<c:url value='/removeContact/${contact.contactId}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
    
	
</body>
</html>