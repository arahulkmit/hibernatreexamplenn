<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Stock Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
    </style>
</head>
<body>
<h1>
    Add a Stock
</h1>
 
<c:url var="addAction" value="/person/add" ></c:url>
 
<form:form action="${addAction}" commandName="stock">

<table>
    <c:if test="${!empty stock.stockName}">
    <tr>
        <td>
            <form:label path="stockId">
                <spring:message text="stockId"/>
            </form:label>
        </td>
        <td>
            <form:input path="stockId" readonly="true" size="8"  disabled="true" />
            <form:hidden path="stockId" />
        </td> 
    </tr>
    </c:if>
    <tr>
        <td>
            <form:label path="stockName">
                <spring:message text="Stock Name :"/>
            </form:label>
        </td>
        <td>
            <form:input path="stockName" />
        </td> 
    </tr>
    <tr>
        <td>
            <form:label path="stockCode">
                <spring:message text="Stock Code: "/>
            </form:label>
        </td>
        <td>
            <form:input path="stockCode" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <c:if test="${!empty stock.stockName}">
                <input type="submit"
                    value="<spring:message text="Edit Stock"/>" />
            </c:if>
            <c:if test="${empty stock.stockName}">
                <input type="submit"
                    value="<spring:message text="Add Stock"/>" />
            </c:if>
        </td>
    </tr>
</table>  
</form:form>
<br>
<h3>Stock List</h3>
<c:if test="${!empty listStocks}">
    <table class="tg">
    <tr>
        <th width="80">Stock ID</th>
        <th width="120">Stock Name</th>
        <th width="120">Stock Code</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <c:forEach items="${listStocks}" var="stock">
        <tr>
            <td>${stock.stockId}</td>
            <td>${stock.stockName}</td>
            <td>${stock.stockCode}</td>
            <td><a href="<c:url value='/edit/${stock.stockId}' />" >Edit</a></td>
            <td><a href="<c:url value='/remove/${stock.stockId}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
</body>
</html>