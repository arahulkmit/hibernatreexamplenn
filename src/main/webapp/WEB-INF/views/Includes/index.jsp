<%-- 
    Document   : index
    Created on : Aug 11, 2015, 12:21:07 PM
    Author     : S522568
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>OneUI - Admin Dashboard Template & UI Framework</title>
        <jsp:include page="head.jsp" />        
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available Classes:

            'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Side Overlay-->
            <jsp:include page="header.jsp"></jsp:include>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <jsp:include page="Left-Side-Menu-Content.jsp" />
            <!-- END Sidebar -->

            <!-- Header -->
            <!-- header and right side overlay is both in header.jsp -->
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
               dfas;kjf;lasd asdfkjf; klasdjl;fkajsd;klf
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <jsp:include page="footer.jsp" />
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Apps Modal -->
        <!-- Opens from the button in the header -->
        <!-- the code required for this will be in the header jsp -->
        <!-- END Apps Modal -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <jsp:include page="scripts.jsp" />
    </body>
</html>
