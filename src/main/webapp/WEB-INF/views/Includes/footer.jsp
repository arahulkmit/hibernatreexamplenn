<%-- 
    Document   : footer
    Created on : Aug 11, 2015, 12:48:09 PM
    Author     : S522568
--%>

<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="pull-right">
        Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
    </div>
    <div class="pull-left">
        <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">OneUI 1.1</a> &copy; <span class="js-year-copy"></span>
    </div>
</footer>