<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>

    <title>Event Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
    </style>
    
    <script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('eventType');
 if(val=='----Select----' || val=='others')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
</head>
<body>
<h1>
    Add an Event
</h1>
 
<c:url var="addAction" value="/event/add" ></c:url>
	
<form:form action="${addAction}" commandName="event">

<table>
    <c:if test="${!empty event.eventName}">
    <tr>
        <td>
            <form:label path="eventId">
                <spring:message text="eventId"/>
            </form:label>
        </td>
        <td>
            <form:input path="eventId" readonly="true" size="8"  disabled="true" />
            <form:hidden path="eventId" />
        </td> 
    </tr>
    </c:if>
    <tr>
        <td>
            <form:label path="eventName">
                <spring:message text="Event Name :"/>
            </form:label>
        </td>
        <td>
            <form:input path="eventName" />
        </td> 
    </tr>
    <tr>
        <td>
            <form:label path="eventDescription">
                <spring:message text="Event Description: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventDescription" />
        </td>
    </tr> 
   <tr>
        <td>
            <form:label path="eventType">
                <spring:message text="Event Type: "/>
            </form:label>
        </td>
        <td>
            <!-- <form:input path="eventType" /> -->
            <select name="eventType" onchange='CheckColors(this.value);'>
            	<option>----Select----</option>
            	<option>general meeting</option>
            	<option>Coffee meeting</option>
            	<option>function</option>
            	<option>others</option>
            </select>            
    		<input type="text" name="eventType" id="eventType" style='display:none;'/>
			
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventInviteContacts">
                <spring:message text="Invite Contacts: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventInviteContacts" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventRemindContacts">
                <spring:message text="Remind Contacts: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventRemindContacts" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventNotes">
                <spring:message text="Add Notes: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventNotes" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventTasks">
                <spring:message text="Add or Schedule Tasks: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventTasks" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2">
            <c:if test="${!empty event.eventName}">
                <input type="submit"
                    value="<spring:message text="Edit Event"/>" />
            </c:if>
            <c:if test="${empty event.eventName}">
                <input type="submit"
                    value="<spring:message text="Add Event"/>" />
            </c:if>
        </td>
    </tr>
</table>  
</form:form>
<br>
<h3>Event List</h3>
<c:if test="${!empty listEvents}">
    <table class="tg">
    <tr>
        <th width="80">Event ID</th>
        <th width="120">Event Name</th>
        <th width="120">Event Description</th>
        <th width="120">Event Type</th>
        <th width="120">Invited Contacts</th>
        <th width="120">Reminded Contacts</th>
        <th width="120">Notes</th>
        <th width="120">Tasks</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>        
    <c:forEach items="${listEvents}" var="event">
        <tr>
            <td>${event.eventId}</td>
            <td>${event.eventName}</td>
            <td>${event.eventDescription}</td>         
            <td>${event.eventType}</td>
            <td>${event.eventInviteContacts}</td>
            <td>${event.eventRemindContacts}</td>
            <td>${event.eventNotes}</td>
            <td>${event.eventTasks}</td>
            <td><a href="<c:url value='/editEvent/${event.eventId}' />" >Edit</a></td>
            <td><a href="<c:url value='/removeEvent/${event.eventId}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
</body>
</html>
</body>

