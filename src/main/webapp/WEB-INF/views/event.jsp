<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>

    <title>Event Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
    </style>   
    <jsp:include page="Includes/head.jsp" />
    <script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('eventType');
 if(val=='----Select----' || val=='others')
   element.style.display='block';
 else  
   element.style.display='none';
}</script>


 
<style>
table tr td{
    padding : 5px;
}
</style>
</head>

<body>


<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
		<!-- Side Overlay-->
		<jsp:include page="Includes/header.jsp" />
		<!-- END Side Overlay -->

		<!-- Sidebar -->
		<jsp:include page="Includes/Left-Side-Menu-Content.jsp" />
		<!-- END Sidebar -->

		<!-- Header -->
		<!-- header and right side overlay is both in header.jsp -->
		<!-- END Header -->

		<!-- Main Container -->
		<div id="main-container">
		 <c:url var="addAction"
			value="/template/add"></c:url>
			<br/>

<div align="center">
<h1>
    Add An Event
</h1>
<br/>
 
<c:url var="addAction" value="/event/add" ></c:url>
	
<form:form action="${addAction}" commandName="event">

<table>
    <c:if test="${!empty event.eventName}">
    <tr>
        <td>
            <form:label path="eventId">
                <spring:message text="eventId"/>
            </form:label>
        </td>
        <td>
            <form:input path="eventId" readonly="true" size="8"  disabled="true" />
            <form:hidden path="eventId" />
        </td> 
    </tr>
    </c:if>
    <tr>
        <td>
            <form:label path="eventName">
                <spring:message text="Event Name :"/>
            </form:label>
        </td>
        <td>
            <form:input path="eventName" />
        </td> 
    </tr>
    <tr>
        <td style="margin-bottom: 50px">
            <form:label path="eventDescription">
                <spring:message text="Event Description: "/>
            </form:label>
        </td>
        <td style="margin-bottom: 50px">
            <form:input path="eventDescription" />
        </td>
    </tr> 
   <tr>
        <td>
            <form:label path="eventType">
                <spring:message text="Event Type: "/>
            </form:label>
        </td>
        <td style="padding: 10px">
            <!-- <form:input path="eventType" /> -->
            <select name="eventType" onchange='CheckColors(this.value);'>
            	<option>----Select----</option>
            	<option>1 on 1</option>
            	<option>2 on 1</option>
            	<option>Phone Call</option>
            	<option>Team Meeting</option>
            	<option>Regional Event</option>
            	<option>National Event</option>
            	<option>others</option>
            </select>            
    		<input type="text" name="eventType" id="eventType" style='display:none;'/>
			
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventLocation">
                <spring:message text="Event Location: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventLocation" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventInviteContacts">
                <spring:message text="Invite Contacts: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventInviteContacts" />
        </td>
    </tr>
    
    <tr>
        <td> 
            <form:label path="eventDate">
                <spring:message text="Event Date: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventDate"/>
        </td>
    </tr>
    
    <tr>
        <td>
            <form:label path="eventRemindContacts">
                <spring:message text="Remind Contacts: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventRemindContacts" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="eventNotes">
                <spring:message text="Add Notes: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventNotes" />
        </td>
    </tr>
    <tr >
        <td> 
            <form:label path="eventTasks">
                <spring:message text="Add or Schedule Tasks: "/>
            </form:label>
        </td>
        <td>
            <form:input path="eventTasks" />
        </td>
    </tr>
    
    
    <tr>
    	<td></td>
        <td colspan="2">
            <c:if test="${!empty event.eventName}">
                <input type="submit"
                    value="<spring:message text="Edit Event"/>" />
            </c:if>
            <c:if test="${empty event.eventName}">
                <input type="submit"
                    value="<spring:message text="Add Event"/>" />
            </c:if>
        </td>
    </tr>
</table>  
</form:form>
<br>
<h3>Event List</h3>
<br/>
<c:if test="${!empty listEvents}">
    <table class="tg">
    <tr>
        <th width="80">Event ID</th>
        <th width="120">Event Name</th>
        <th width="120">Event Description</th>
        <th width="120">Event Type</th>
        <th width="120">Event Location</th>
        <th width="120">Invited Contacts</th>
        <th width="120">Reminded Contacts</th>
        <th width="120">Notes</th>
        <th width="120">Date</th>
        <th width="120">Tasks</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>        
    <c:forEach items="${listEvents}" var="event">
        <tr>
            <td>${event.eventId}</td>
            <td>${event.eventName}</td>
            <td>${event.eventDescription}</td>         
            <td>${event.eventType}</td>
            <td>${event.eventLocation}</td>
            <td>${event.eventInviteContacts}</td>
            <td>${event.eventRemindContacts}</td>
            <td>${event.eventNotes}</td>
            <td>${event.eventDate}</td>
            <td>${event.eventTasks}</td>
            <td><a href="<c:url value='/editEvent/${event.eventId}' />" >Edit</a></td>
            <td><a href="<c:url value='/removeEvent/${event.eventId}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
</div>
</div>
<!-- END Main Container -->
		<!-- Footer -->
		<jsp:include page="Includes/footer.jsp"></jsp:include>
		<!-- END Footer -->
	</div>
	<!-- END Page Container -->

	<!-- Apps Modal -->
	<!-- Opens from the button in the header -->
	<!-- the code required for this will be in the header jsp -->
	<!-- END Apps Modal -->

	<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
	<jsp:include page="Includes/scripts.jsp" />  

</body>


</html>


