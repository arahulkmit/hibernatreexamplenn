<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Contact</title>
</head>
<body>
 <c:url var="addAction" value="/mailList/add" ></c:url>
	<h1> Contacts</h1>	
	<form:form action="${addAction}" commandName="mailList">
	<table>
	<c:if test="${!empty mailList.mailListName}">
    <tr>
        <td>
            <form:label path="mailListID">
                <spring:message text="mailListID"/>
            </form:label>
        </td>
        <td>
            <form:input path="mailListID" readonly="true" size="8"  disabled="true" />
            <form:hidden path="mailListID" />
        </td> 
    </tr>
    </c:if>
    
     <tr>
        <td>
            <form:label path="mailListName">
                <spring:message text="Mail List Name :"/>
            </form:label>
        </td>
        <td>
            <form:input path="mailListName" />
        </td> 
    </tr>
    <tr>
        <td>
            <form:label path="MailListNotes">
                <spring:message text="Mail List Notes :"/>
            </form:label>
        </td>
        <td>
            <form:input path="MailListNotes" />
        </td> 
    </tr>
<!--     <tr> -->
<!--         <td> -->
<%--             <form:label path="MailListNotes"> --%>
<%--                 <spring:message text="Mail List Notes :"/> --%>
<%--             </form:label> --%>
<!--         </td> -->
<!--         <td> -->
<%--             <form:input path="MailListNotes" /> --%>
<!--         </td>  -->
<!--     </tr> -->
    <tr>
        <td>
            <form:label path="contactID">
                <spring:message text="mailListID:"/>
            </form:label>
        </td>
        <td>
            <form:input id="setContactID" path="contactID" />
        </td> 
    </tr>
   
    
    <tr>
        <td colspan="2">
            <c:if test="${!empty mailList.mailListName}">
                <input type="submit"
                    value="<spring:message text="Edit Contact"/>" />
            </c:if>
            <c:if test="${empty mailList.mailListName}">
                <input type="submit"
                    value="<spring:message text="Add Contact"/>" />
            </c:if>
        </td>
    </tr>
</table>  
</form:form>
<br>

<h3>Contact List</h3>
<c:if test="${!empty listContacts}">
    <table class="tg">
    <tr>
        <th width="80">Contact ID</th>
        <th width="120">Contact Name</th>
        <th width="120">Contact Email</th>
        <th width="120">Contact Number</th>
        <th width="120">Contact Skype</th>
        <th width="120">Add</th>
        
    </tr>
    <c:forEach items="${listContacts}" var="contact">
        <tr>
            <td>${contact.contactId}</td>
            <td><a href="" id= ${contact.contactId} onClick="reply_click(this.id)">${contact.contactName}</a></td>
            <td>${contact.contactEmail}</td>
            <td>${contact.contactNumber}</td>
            <td>${contact.skypeId}</td>
            <td><input id= ${contact.contactId} type="button" name="name_ADD"  value="ADD" onClick="reply_click(this.id)"></td>                      
        </tr>
    </c:forEach>
    </table>
</c:if>

 <script type="text/javascript">
 var i=0;
 var id= "";
function reply_click(clicked_id)
{
	i++;	
	id += "   "+clicked_id;
	document.getElementById('setContactID').value = id ;	
    //alert(id);   
}
</script>

<h3>Mail List</h3>
<c:if test="${!empty listMailList}">
    <table class="tg">
    <tr>
        <th width="80">Mail list ID</th>
        <th width="120">Contact Id s</th>        
        <th width="120">Mail List Name</th>
        <th width="120">Mail List Notes</th>
        <th width="120">userID</th>
        <th width="120">Edit</th>
        <th width="120">Delete</th>
    </tr>
    <c:forEach items="${listMailList}" var="listmaillist">
        <tr>
            <td>${listmaillist.mailListID}</td>
            <td>${listmaillist.contactID}</td>
            <td>${listmaillist.mailListName}</td>            
            <td>${listmaillist.mailListNotes}</td>            
            <td>${listmaillist.userID}</td>
             <td><a href="<c:url value='/editMailList/${listmaillist.mailListID}' />" >Edit</a></td>
           <td><a href="<c:url value='/removeMailList/${listmailList.mailListID}' />" >Delete</a></td>                     
        </tr>
    </c:forEach>
    </table>
</c:if>
	
</body>
</html>