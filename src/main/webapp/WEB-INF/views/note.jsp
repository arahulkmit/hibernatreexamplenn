<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Notes Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
    </style>
    <jsp:include page="Includes/head.jsp" />
    <script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('Type');
 if(val=='----Select----' || val=='others')
   element.style.display='block';
 else  
   element.style.display='none';
}
</script> 
<style>
table tr td{
    padding : 5px;
}
</style>
</head>
<body>

<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
		<!-- Side Overlay-->
		<jsp:include page="Includes/header.jsp" />
		<!-- END Side Overlay -->

		<!-- Sidebar -->
		<jsp:include page="Includes/Left-Side-Menu-Content.jsp" />
		<!-- END Sidebar -->

		<!-- Header -->
		<!-- header and right side overlay is both in header.jsp -->
		<!-- END Header -->

		<!-- Main Container -->
		<div id="main-container">
		 <c:url var="addAction"
			value="/template/add"></c:url>
			<br/>

<div align="center">
<h1>
    Add Notes
</h1>
<br/>
 
<c:url var="addAction" value="/note/add" ></c:url>
	
<form:form action="${addAction}" commandName="note">

<table>
    <c:if test="${!empty note.notesType}">
    <tr>
        <td>
            <form:label path="notesId">
                <spring:message text="notesId"/>
            </form:label>
        </td>
        <td>
            <form:input path="notesId" readonly="true" size="8"  disabled="true" />
            <form:hidden path="notesId" />
        </td> 
    </tr>
    </c:if>
    
     <tr>
        <td>
            <form:label path="notesType">
                <spring:message text="Notes Type: "/>
            </form:label>
        </td>
        <td style="padding: 10px">
            <!-- <form:input path="notesType" /> -->
            <select name="notesType" onchange='CheckColors(this.value);'>
            	<option>----Select----</option>
            	<option>Event Notes</option>
            	<option>Contact Notes</option>
            	<option>General Notes</option>
            	<option>others</option>
            </select>            
    		<input type="text" name="notesType" id="notesType" style='display:none;'/>
			
        </td>
    </tr>
    
    <tr>
        <td>
            <form:label path="notesDescription">
                <spring:message text="Notes Description: "/>
            </form:label>
        </td>
        <td>
            <form:input path="notesDescription" style="height:120;width:350;" />
        </td>
    </tr> 
    
    <tr>
    	<td></td>
        <td colspan="2">
            <c:if test="${!empty event.eventType}">
                <input type="submit"
                    value="<spring:message text="Edit Event"/>" />
            </c:if>
            <c:if test="${empty event.eventType}">
                <input type="submit"
                    value="<spring:message text="Add Event"/>" />
            </c:if>
        </td>
    </tr>
  
</table>  
</form:form>
<br>
<h3>Notes List</h3>
<br/>
<c:if test="${!empty listNotes}">
    <table class="tg">
    <tr>
        <th width="80">Notes ID</th>
        <th width="120">Notes Type</th>
        <th width="120">Notes Description</th>        
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>        
    <c:forEach items="${listNotes}" var="note">
        <tr>
            <td>${note.notesId}</td>
            <td>${note.notesType}</td>
            <td>${note.notesDescription}</td>         
            <td><a href="<c:url value='/editNotes/${note.notesId}' />" >Edit</a></td>
            <td><a href="<c:url value='/removeNotes/${note.notesId}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
</div>
</div>
<!-- END Main Container -->
		<!-- Footer -->
		<jsp:include page="Includes/footer.jsp"></jsp:include>
		<!-- END Footer -->
	</div>
	<!-- END Page Container -->

	<!-- Apps Modal -->
	<!-- Opens from the button in the header -->
	<!-- the code required for this will be in the header jsp -->
	<!-- END Apps Modal -->

	<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
	<jsp:include page="Includes/scripts.jsp" />  

</body>
</html>
</body>

