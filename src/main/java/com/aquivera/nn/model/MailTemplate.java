package com.aquivera.nn.model;
	
	/*
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */
	

	import java.io.Serializable;
	import javax.persistence.Basic;
	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.Lob;
	import javax.persistence.NamedQueries;
	import javax.persistence.NamedQuery;
	import javax.persistence.Table;
	import javax.validation.constraints.Size;
	import javax.xml.bind.annotation.XmlRootElement;

	/**
	 *
	 * @author S522568
	 */
	@Entity
	@Table(name = "mailtemplate")
	@XmlRootElement
//	@NamedQueries({
//	    @NamedQuery(name = "Mailtemplate.findAll", query = "SELECT m FROM Mailtemplate m"),
//	    @NamedQuery(name = "Mailtemplate.findByMailTemplateID", query = "SELECT m FROM Mailtemplate m WHERE m.mailTemplateID = :mailTemplateID")})
	public class MailTemplate implements Serializable {
	    private static final long serialVersionUID = 1L;
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false) 
	    @Column(name = "MailTemplateID")
	    private Integer mailTemplateID;
	    @Lob
	    @Size(max = 16777215)
	    @Column(name = "MailTemplateName")
	    private String mailTemplateName;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailTemplateSubject")
	    private String mailTemplateSubject;
	    @Lob
	    @Column(name = "MailImage")
	    private byte[] mailImage;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailGreetings")
	    private String mailGreetings;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailHeader")
	    private String mailHeader;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailBody")
	    private String mailBody;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailFooter")
	    private String mailFooter;
	    @Lob
	    @Size(max = 2147483647)
	    @Column(name = "MailSignature")
	    private String mailSignature;

	    public MailTemplate() {
	    }

	    public MailTemplate(Integer mailTemplateID) {
	        this.mailTemplateID = mailTemplateID;
	    }

	    public Integer getMailTemplateID() {
	        return mailTemplateID;
	    }

	    public void setMailTemplateID(Integer mailTemplateID) {
	        this.mailTemplateID = mailTemplateID;
	    }

	    public String getMailTemplateName() {
	        return mailTemplateName;
	    }

	    public void setMailTemplateName(String mailTemplateName) {
	        this.mailTemplateName = mailTemplateName;
	    }

	    public String getMailTemplateSubject() {
	        return mailTemplateSubject;
	    }

	    public void setMailTemplateSubject(String mailTemplateSubject) {
	        this.mailTemplateSubject = mailTemplateSubject;
	    }

	    public byte[] getMailImage() {
	        return mailImage;
	    }

	    public void setMailImage(byte[] mailImage) {
	        this.mailImage = mailImage;
	    }

	    public String getMailGreetings() {
	        return mailGreetings;
	    }

	    public void setMailGreetings(String mailGreetings) {
	        this.mailGreetings = mailGreetings;
	    }

	    public String getMailHeader() {
	        return mailHeader;
	    }

	    public void setMailHeader(String mailHeader) {
	        this.mailHeader = mailHeader;
	    }

	    public String getMailBody() {
	        return mailBody;
	    }

	    public void setMailBody(String mailBody) {
	        this.mailBody = mailBody;
	    }

	    public String getMailFooter() {
	        return mailFooter;
	    }

	    public void setMailFooter(String mailFooter) {
	        this.mailFooter = mailFooter;
	    }

	    public String getMailSignature() {
	        return mailSignature;
	    }

	    public void setMailSignature(String mailSignature) {
	        this.mailSignature = mailSignature;
	    }

	    @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (mailTemplateID != null ? mailTemplateID.hashCode() : 0);
	        return hash;
	    }

	    @Override
	    public boolean equals(Object object) {
	        // TODO: Warning - this method won't work in the case the id fields are not set
	        if (!(object instanceof MailTemplate)) {
	            return false;
	        }
	        MailTemplate other = (MailTemplate) object;
	        if ((this.mailTemplateID == null && other.mailTemplateID != null) || (this.mailTemplateID != null && !this.mailTemplateID.equals(other.mailTemplateID))) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        return "test.Mailtemplate[ mailTemplateID=" + mailTemplateID + " ]";
	    }
	    
	}



