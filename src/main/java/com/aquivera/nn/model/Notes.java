/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aquivera.nn.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author S521946
 */
@Entity
@Table(name = "notes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notes.findAll", query = "SELECT n FROM Notes n"),
    @NamedQuery(name = "Notes.findByNotesId", query = "SELECT n FROM Notes n WHERE n.notesId = :notesId"),
    @NamedQuery(name = "Notes.findByNotesType", query = "SELECT n FROM Notes n WHERE n.notesType = :notesType"),
    @NamedQuery(name = "Notes.findByNotesDescription", query = "SELECT n FROM Notes n WHERE n.notesDescription = :notesDescription")})
public class Notes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "notes_id")
    private Integer notesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "notes_type")
    private String notesType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "notes_description")
    private String notesDescription;

    public Notes() {
    }

    public Notes(Integer notesId) {
        this.notesId = notesId;
    }

    public Notes(Integer notesId, String notesType, String notesDescription) {
        this.notesId = notesId;
        this.notesType = notesType;
        this.notesDescription = notesDescription;
    }

    public Integer getNotesId() {
        return notesId;
    }

    public void setNotesId(Integer notesId) {
        this.notesId = notesId;
    }

    public String getNotesType() {
        return notesType;
    }

    public void setNotesType(String notesType) {
        this.notesType = notesType;
    }

    public String getNotesDescription() {
        return notesDescription;
    }

    public void setNotesDescription(String notesDescription) {
        this.notesDescription = notesDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notesId != null ? notesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notes)) {
            return false;
        }
        Notes other = (Notes) object;
        if ((this.notesId == null && other.notesId != null) || (this.notesId != null && !this.notesId.equals(other.notesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.aquivera.nn.Notes[ notesId=" + notesId + " ]";
    }
    
}
