/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aquivera.nn.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author S522568
 */
@Entity
@Table(name = "maillist")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Maillist.findAll", query = "SELECT m FROM Maillist m"),
//    @NamedQuery(name = "Maillist.findByMailListID", query = "SELECT m FROM Maillist m WHERE m.mailListID = :mailListID"),
//    @NamedQuery(name = "Maillist.findByContactID", query = "SELECT m FROM Maillist m WHERE m.contactID = :contactID"),
//    @NamedQuery(name = "Maillist.findByUserID", query = "SELECT m FROM Maillist m WHERE m.userID = :userID")})
public class MailList implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MailListID")
    private Integer mailListID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contactID")
    private int contactID;
    @Lob
    @Size(max = 65535)
    @Column(name = "MailListName")
    private String mailListName;
    @Lob
    @Size(max = 16777215)
    @Column(name = "MailListNotes")
    private String mailListNotes;
    @Column(name = "userID")
    private Integer userID;

    public MailList() {
    }

    public MailList(Integer mailListID) {
        this.mailListID = mailListID;
    }

    public MailList(Integer mailListID, int contactID) {
        this.mailListID = mailListID;
        this.contactID = contactID;
    }

    public Integer getMailListID() {
        return mailListID;
    }

    public void setMailListID(Integer mailListID) {
        this.mailListID = mailListID;
    }

    public int getContactID() {
        return contactID;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }

    public String getMailListName() {
        return mailListName;
    }

    public void setMailListName(String mailListName) {
        this.mailListName = mailListName;
    }

    public String getMailListNotes() {
        return mailListNotes;
    }

    public void setMailListNotes(String mailListNotes) {
        this.mailListNotes = mailListNotes;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mailListID != null ? mailListID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MailList)) {
            return false;
        }
        MailList other = (MailList) object;
        if ((this.mailListID == null && other.mailListID != null) || (this.mailListID != null && !this.mailListID.equals(other.mailListID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Maillist[ mailListID=" + mailListID + " ]";
    }
    
}
