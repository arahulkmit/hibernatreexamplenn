package com.aquivera.nn.model;

import java.util.ArrayList;
import java.util.List;

public class Stockk {

	
	Integer stockId;
	String stockName;
	String stockCode;
	
	List<String> name = new ArrayList<String>();
	
	
	
	public Integer getStockId() {
		return stockId;
	}
 
	public String getStockName() {
		return stockName;
	}
	 
	public String getStockCode() {
		return stockCode;
	}
	 
	public Stockk(Integer stockId, String stockName, String stockCode,List<String> name) {
		
		this.stockId = stockId;
		this.stockName = stockName;
		this.stockCode = stockCode;
		this.name=name;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}
	
	
}
