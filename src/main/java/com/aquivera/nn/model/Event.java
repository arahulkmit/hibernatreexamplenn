
package com.aquivera.nn.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.standard.DateTimeContext;

/**
 *
 * @author PhanindraKumar Boogarapu
 */
@Entity
@Table(name = "event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Event.findAll", query = "SELECT e FROM Event e"),
    @NamedQuery(name = "Event.findByEventId", query = "SELECT e FROM Event e WHERE e.eventId = :eventId"),
    @NamedQuery(name = "Event.findByEventName", query = "SELECT e FROM Event e WHERE e.eventName = :eventName"),
    @NamedQuery(name = "Event.findByEventType", query = "SELECT e FROM Event e WHERE e.eventType = :eventType"),
    @NamedQuery(name = "Event.findByEventinviteContacts", query = "SELECT e FROM Event e WHERE e.eventInviteContacts = :eventInviteContacts"),
    @NamedQuery(name = "Event.findByEventremindContacts", query = "SELECT e FROM Event e WHERE e.eventRemindContacts = :eventRemindContacts"),
    @NamedQuery(name = "Event.findByEventNotes", query = "SELECT e FROM Event e WHERE e.eventNotes = :eventNotes"),
    @NamedQuery(name = "Event.findByEventLocation", query = "SELECT e FROM Event e WHERE e.eventLocation = :eventLocation"),
    @NamedQuery(name = "Event.findByEventaddTasks", query = "SELECT e FROM Event e WHERE e.eventTasks = :eventTasks"),
    @NamedQuery(name = "Event.findByEventDescription", query = "SELECT e FROM Event e WHERE e.eventDescription = :eventDescription")})
	
public class Event implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)  
    @Column(name = "event_id")
    private Integer eventId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45, message="please enter a value for event name for size between 1 to 45 ")
    @Column(name = "event_name")
    private String eventName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "event_description")
    private String eventDescription;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "event_type")
    private String eventType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "invite_contacts")
    private String eventInviteContacts;
    @Basic(optional = false)                  
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "remind_contacts")
    private String eventRemindContacts;
    @Basic(optional = false)        
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "add_notes")
    private String eventNotes;
    @Basic(optional = false)        
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "event_Location")
    private String eventLocation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "add_tasks")
    private String eventTasks;
    @Column(name = "event_date")
    @Temporal(TemporalType.DATE)
    private Date eventDate;

	public Event() {
    }

    public Event(Integer eventId) {
        this.eventId = eventId;
    }

    public Event(Integer eventId, String eventName, String eventDescription) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
    }

    public Integer getEventId() {
        return eventId;
    }

    public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }
    
    public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}	

    public String getEventInviteContacts() {
		return eventInviteContacts;
	}

	public void setEventInviteContacts(String eventInviteContacts) {
		this.eventInviteContacts = eventInviteContacts;
	}

	public String getEventRemindContacts() {
		return eventRemindContacts;
	}

	public void setEventRemindContacts(String eventRemindContacts) {
		this.eventRemindContacts = eventRemindContacts;
	}

	public String getEventNotes() {
		return eventNotes;
	}

	public void setEventNotes(String eventNotes) {
		this.eventNotes = eventNotes;
	}

	public String getEventTasks() {
		return eventTasks;
	}

	public void setEventTasks(String eventTasks) {
		this.eventTasks = eventTasks;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (eventId != null ? eventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.eventId == null && other.eventId != null) || (this.eventId != null && !this.eventId.equals(other.eventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.aquivera.nn.model.Event[ eventId=" + eventId + " ]";
    }
    
}
