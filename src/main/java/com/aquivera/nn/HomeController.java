package com.aquivera.nn;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aquivera.nn.model.Contact;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
//	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
//	public String contactsForm(Model model) {
//		
//		model.addAttribute("contact",new Contact());
//		
//		return "contact";
//	}
	
	
//	@RequestMapping(value = "/contacts", method = RequestMethod.POST)
//	public String contactsSubmit(@Valid Contact contact,BindingResult bindingResult,Model model) {
//		
//		System.out.println(bindingResult.hasErrors());
//		
//		if (bindingResult.hasErrors()) {
//            logger.info("Returning custSave.jsp page");
//            return "contact";
//        }
//		
//		model.addAttribute("contact",contact);
//		
//		return "result";
//		
//	}
	
	
	
	
}
