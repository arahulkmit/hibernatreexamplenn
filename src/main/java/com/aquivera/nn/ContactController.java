package com.aquivera.nn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.Stock;
import com.aquivera.nn.service.ContactService;
import com.aquivera.nn.service.StockService;


@Controller
public class ContactController {

	private ContactService contactService; 

	

	@Autowired(required = true)
	@Qualifier(value = "contactService")
	public void setContactService(ContactService contactService) {
		this.contactService = contactService;
	}

//	@RequestMapping(value = "/contact/add", method = RequestMethod.GET)
//	public String addContact(@ModelAttribute("contact") Contact contact) {
//
//		this.contactService.addContact(contact);
//
//		return "redirect:/contacts";
//
//	}
	
	 @RequestMapping(value = "/contacts", method = RequestMethod.GET)
	    public String listcontacts(Model model) {
	        model.addAttribute("contact", new Contact());
	        model.addAttribute("listContacts", this.contactService.listContacts());
	        return "contacts";
	    }
	     
	    //For add and update person both
	    @RequestMapping(value= "/contact/add", method = RequestMethod.POST)
	    public String addContact(@ModelAttribute("contact") Contact contact){
	    	System.out.println(contact.getContactName());
	       if(contact!=null && contact.getContactId() == null){
	            //new person, add it
	            this.contactService.addContact(contact);
	        }else{
	            //existing person, call update
	            this.contactService.updateContact(contact);
	        }
	         
	        return "redirect:/contacts";
	         
	    }
	     
	    @RequestMapping("/removeContact/{id}")
	    public String removeStock(@PathVariable("id") int contactId){
	         
	        this.contactService.removeContact(contactId);
	        return "redirect:/contacts";
	    }
	  
	    @RequestMapping("/editContact/{id}")
	    public String editStock(@PathVariable("id") int contactId, Model model){
	        model.addAttribute("contact", this.contactService.getContactById(contactId));
	        model.addAttribute("listContacts", this.contactService.listContacts());
	        return "contacts";
	    }
}
