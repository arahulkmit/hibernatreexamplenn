package com.aquivera.nn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.MailList;
import com.aquivera.nn.service.ContactService;
import com.aquivera.nn.service.MailListService;

@Controller
public class MailListController {
	  
	private ContactService contactService;
	private MailListService mailListService;
	
	@Autowired(required = true)
	@Qualifier(value = "contactService")
	public void setContactService(ContactService contactService) {
		this.contactService = contactService;
	}
	
	@Autowired(required = true)
	@Qualifier(value = "mailListService")
	public void setMailListService(MailListService mailListService) {
		this.mailListService = mailListService;
	}
	
	 @RequestMapping(value = "/mailList", method = RequestMethod.GET)
	    public String listcontacts(Model model) {
	        model.addAttribute("contact", new Contact());
	        model.addAttribute("listContacts", this.contactService.listContacts());
	        model.addAttribute("mailList", new MailList());
	        model.addAttribute("listMailList", this.mailListService.listMailList());
	        return "mailList";
	    } 
	 
	 //For add and update mail list both in one method
	    @RequestMapping(value= "/mailList/add", method = RequestMethod.POST)
	    public String addMailList(@ModelAttribute("mailList") MailList mailList){
	    	System.out.println(mailList.getMailListName());
	       if(mailList!=null && mailList.getMailListID() == null){
	            //new mail list, add it
	            this.mailListService.addMailList(mailList);
	        }else{
	            //existing mail list, mail list update update
	            this.mailListService.updateMailList(mailList);
	        }
	         
	        return "redirect:/mailList";
	         
	    }
	    
	    // for removing mail list
	    @RequestMapping("/removeMailList/{id}")
	    public String removeStock(@PathVariable("id") int mailListID){
	         
	        this.mailListService.removeMailList(mailListID);
	        return "redirect:/mailList";
	    }
	    
	    @RequestMapping("/editMailList/{id}")
	    public String editStock(@PathVariable("id") int mailListID, Model model){
	        model.addAttribute("mailList", this.mailListService.getMailListById(mailListID));
	        model.addAttribute("listMailList", this.mailListService.listMailList());
	        return "redirect:/mailList";
	    }
	    
}
