package com.aquivera.nn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aquivera.nn.model.Event;
import com.aquivera.nn.model.Stock;
import com.aquivera.nn.service.EventService;;

@Controller
public class EventController {
	
	public EventService eventService;
	
	@Autowired(required=true)
	@Qualifier(value="eventService")
	public void setEventService(EventService eventService){
		this.eventService = eventService;
	}
	
//	@RequestMapping(value = "/event/add", method = RequestMethod.GET)
//	public String addEvent(@ModelAttribute("event") Event s) {
//		this.eventService.addEvent(s);
//
//		return "redirect:/events";
//	}
	
	@RequestMapping(value = "/events", method = RequestMethod.GET)
    public String listEvents(Model model) {
		System.out.println("heeeeeeey");
        model.addAttribute("event", new Event());
        model.addAttribute("listEvents", this.eventService.listEvents());
        return "event";
        
    }
	
	//For add and update person both
    @RequestMapping(value= "/event/add", method = RequestMethod.POST)
    public String addEvent1(@Valid @ModelAttribute("event") Event p){
    	System.out.println(p);
    	
       if(p!=null && p.getEventId() == null){
            //new event, add it
            this.eventService.addEvent(p);
        }else{
            //existing event, call update
            this.eventService.updateEvent(p);
        }
       System.out.println("heoooooooooy"); 
        return "redirect:/events";
         
    }
    
    @RequestMapping(value= "/event/data", method = RequestMethod.POST)
    public String addEvent2(@Valid @ModelAttribute("event") Event p){
    	System.out.println(p);
    	System.out.println("heeeeeeey");
    	
       if(p!=null && p.getEventId() == null){
            //new event, add it
            this.eventService.addEvent(p);
        }else{
            //existing event, call update
            this.eventService.updateEvent(p);
        }
         
        return "redirect:/events";
         
    }
    
    @RequestMapping("/removeEvent/{id}")
    public String removeEvent(@PathVariable("id") int eventid){         
        this.eventService.removeEvent(eventid);
        return "redirect:/events";
    }
	
    @RequestMapping("/editEvent/{id}")
    public String editEvent(@PathVariable("id") int eventid, Model model){
        model.addAttribute("event", this.eventService.getEventById(eventid));
        model.addAttribute("listEvents", this.eventService.listEvents());
        return "event";
    }
	
}
