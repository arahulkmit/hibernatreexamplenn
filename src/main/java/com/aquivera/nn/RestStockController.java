package com.aquivera.nn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aquivera.nn.model.Stock;
import com.aquivera.nn.model.Stockk;
import com.aquivera.nn.service.StockService;



@RestController	

public class RestStockController {

	
	private StockService stockService; 
	
	@Autowired(required = true)
	@Qualifier(value = "stockService")
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}
	
	
	@RequestMapping(value="/getData")  

	public List<Stock> getStockData(
			@RequestParam(value = "name", defaultValue = "Google") String name) {
		
		return this.stockService.listStocks();
	}
}
