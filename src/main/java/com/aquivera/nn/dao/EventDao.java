package com.aquivera.nn.dao;

import java.util.List;

import com.aquivera.nn.model.Event;

public interface EventDao {

	public void addEvent(Event s);
	public void updateEvent(Event p);
    public List<Event> listEvents();
    public Event getEventById(int id);
    public void removeEvent(int id);
	
}
