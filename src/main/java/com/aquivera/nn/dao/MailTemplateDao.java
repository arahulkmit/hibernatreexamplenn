package com.aquivera.nn.dao;

import java.util.List;

import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.MailTemplate;

public interface MailTemplateDao {
	
	public void addMailTemplate(MailTemplate template );
	public void updateMailTemplate(MailTemplate template);
    public List<MailTemplate> listMailTemplate();
    public MailTemplate getMailTemplateById(int mailTemplateID);
    public void removeMailTemplate(int mailTemplateID);

}
  