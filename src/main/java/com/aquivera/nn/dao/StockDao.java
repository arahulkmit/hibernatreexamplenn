package com.aquivera.nn.dao;

import java.util.List;

import com.aquivera.nn.model.Stock;

public interface StockDao {

	public void addStock(Stock s);
	public void updateStock(Stock p);
    public List<Stock> listStocks();
    public Stock getStockById(int id);
    public void removeStock(int id);
	
}
