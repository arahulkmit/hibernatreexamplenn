package com.aquivera.nn.dao;

import java.util.List;

import com.aquivera.nn.model.Notes;;

public interface NotesDao {

	public void addNotes(Notes s);
	public void updateNotes(Notes p);
    public List<Notes> listNotes();
    public Notes getNotesById(int id);
    public void removeNotes(int id);
}
	