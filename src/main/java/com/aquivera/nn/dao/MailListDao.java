package com.aquivera.nn.dao;

import java.util.List;
import com.aquivera.nn.model.MailList;


public interface MailListDao {

	public void addMailList(MailList mailList);
	public void updateMailList(MailList mailList);
    public List<MailList> listMailList();
    public MailList getMailListById(int mailListID);
    public void removeMailList(int mailListID);
    
	
}
