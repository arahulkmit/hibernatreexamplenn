package com.aquivera.nn.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.ContactDao;
import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.Stock;

public class ContactDaoImpl implements ContactDao{

	private static final Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
	
	
	@Override
	@Transactional
	public void addContact(Contact contact) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(contact);
        logger.info("contact saved successfully, contact Details="+contact);
		
	}

	@Override
	@Transactional
	public void updateContact(Contact contact) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(contact);
		logger.info("Contact updated successfully!");
		
	}

	@Override
	@Transactional
	public List<Contact> listContacts() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        List<Contact> contactList = session.createQuery("from Contact").list();
        for(Contact contact : contactList){
            logger.info("Contact List:: "+contact);
        }
        return contactList;
		
	}

	@Override
	@Transactional
	public Contact getContactById(int contactId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();      
        Contact contact = (Contact) session.load(Contact.class, new Integer(contactId));
        logger.info("Contact loaded successfully, Contact details="+contact);
        return contact;
	}

	@Override
	@Transactional
	public void removeContact(int contactId) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        Contact count = (Contact) session.load(Contact.class, new Integer(contactId));
        if(null != count){
            session.delete(count);
        }
        logger.info("Contact deleted successfully, contact details="+count);
		
		
	}

	
}
