package com.aquivera.nn.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.NotesDao;
import com.aquivera.nn.model.Notes;;

public class NotesDaoImpl implements NotesDao{

	private static final Logger logger = LoggerFactory.getLogger(NotesDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
 
	
    @Override
    @Transactional
	public void addNotes(Notes s) {
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(s);
        logger.info("Notes saved successfully, Notes Details="+s);
		
	}


	@Override
	@Transactional
	public void updateNotes(Notes p) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Notes updated successfully!");
		
	}


	@Override
	@Transactional
	public List<Notes> listNotes() {
		Session session = this.sessionFactory.getCurrentSession();
        List<Notes> NotesList = session.createQuery("from Notes").list();
        for(Notes p : NotesList){
            logger.info("Notes List::"+p);
        }
        return NotesList;
	}


	@Override
	@Transactional
	public Notes getNotesById(int id) {
		Session session = this.sessionFactory.getCurrentSession();      
        Notes p = (Notes) session.load(Notes.class, new Integer(id));
        logger.info("Notes loaded successfully, Notes details="+p);
        return p;
	}


	@Override
	@Transactional
	public void removeNotes(int id) {
		Session session = this.sessionFactory.getCurrentSession();
        Notes p = (Notes) session.load(Notes.class, new Integer(id));
        if(null != p){
            session.delete(p);
        }
        logger.info("Notes deleted successfully, Notes details="+p);
		
	}

	
}
