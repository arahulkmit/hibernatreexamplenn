package com.aquivera.nn.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.MailTemplateDao;
import com.aquivera.nn.model.MailTemplate;
import com.aquivera.nn.model.MailTemplate;

public class MailTemplateDaoImpl implements MailTemplateDao {
	
	private static final Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

	@Override
	@Transactional
	public void addMailTemplate(MailTemplate template) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(template);
        logger.info("Template saved successfully, template Details="+template);
		
	}

	@Override
	@Transactional
	public void updateMailTemplate(MailTemplate template) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(template);
		logger.info("template updated successfully!");
		
	}

	@Override
	@Transactional
	public List<MailTemplate> listMailTemplate() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        List<MailTemplate> templateList = session.createQuery("from MailTemplate").list();
        for(MailTemplate template : templateList){
            logger.info("Template List:: "+template);
        }
        return templateList;
		
	}

	@Override
	@Transactional
	public MailTemplate getMailTemplateById(int mailTemplateID) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();      
        MailTemplate template = (MailTemplate) session.load(MailTemplate.class, new Integer(mailTemplateID));
        logger.info("template loaded successfully, template details="+template);
        return template;
	}

	@Override
	@Transactional
	public void removeMailTemplate(int mailTemplateID) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        MailTemplate count = (MailTemplate) session.load(MailTemplate.class, new Integer(mailTemplateID));
        if(null != count){
            session.delete(count);
        }
        logger.info("Template deleted successfully, template details="+count);
		
	}
	

}
