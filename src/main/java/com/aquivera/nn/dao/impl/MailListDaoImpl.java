package com.aquivera.nn.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.MailListDao;
import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.MailList;

public class MailListDaoImpl implements MailListDao {
	
	private static final Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
	
	@Override
	@Transactional
	public void addMailList(MailList mailList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(mailList);
        logger.info("MailList saved successfully, MailList Details="+mailList);
		
	}

	@Override
	@Transactional
	public void updateMailList(MailList mailList) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(mailList);
		logger.info("MailList updated successfully!");
	}

	@Override
	@Transactional
	public List<MailList> listMailList() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        List<MailList> ListMailList = session.createQuery("from MailList ").list();
        for(MailList listofmails : ListMailList){
            logger.info("Mail List List:: "+listofmails);
        }
        return ListMailList;
	}

	@Override
	@Transactional
	public MailList getMailListById(int mailListID) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();      
		MailList maillist = (MailList) session.load(MailList.class, new Integer(mailListID));
        logger.info("Mail List loaded successfully, Mail List details="+maillist);
        return maillist;
	}

	@Override
	@Transactional
	public void removeMailList(int mailListID) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        MailList count = (MailList) session.load(MailList.class, new Integer(mailListID));
        if(null != count){
            session.delete(count);
        }
        logger.info("Mail List deleted successfully, Mail List details="+count);
		
	}

}
