package com.aquivera.nn.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.EventDao;
import com.aquivera.nn.model.Event;;

public class EventDaoImpl implements EventDao{

	private static final Logger logger = LoggerFactory.getLogger(EventDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
 
	
    @Override
    @Transactional
	public void addEvent(Event s) {
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(s);
        logger.info("Event saved successfully, Event Details="+s);
		
	}


	@Override
	@Transactional
	public void updateEvent(Event p) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Event updated successfully!");
		
	}


	@Override
	@Transactional
	public List<Event> listEvents() {
		Session session = this.sessionFactory.getCurrentSession();
        List<Event> EventList = session.createQuery("from Event").list();
        for(Event p : EventList){
            logger.info("Event List::"+p);
        }
        return EventList;
	}


	@Override
	@Transactional
	public Event getEventById(int id) {
		Session session = this.sessionFactory.getCurrentSession();      
        Event p = (Event) session.load(Event.class, new Integer(id));
        logger.info("Event loaded successfully, Event details="+p);
        return p;
	}


	@Override
	@Transactional
	public void removeEvent(int id) {
		Session session = this.sessionFactory.getCurrentSession();
        Event p = (Event) session.load(Event.class, new Integer(id));
        if(null != p){
            session.delete(p);
        }
        logger.info("Event deleted successfully, Event details="+p);
		
	}
	
}
