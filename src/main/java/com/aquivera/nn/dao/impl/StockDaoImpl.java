package com.aquivera.nn.dao.impl;

import java.util.List;




import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.aquivera.nn.dao.StockDao;
import com.aquivera.nn.model.Stock;

public class StockDaoImpl implements StockDao{

	private static final Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	 
    private SessionFactory sessionFactory;
     
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
 
	
    @Override
    @Transactional
	public void addStock(Stock s) {
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(s);
        logger.info("Stock saved successfully, Stock Details="+s);
		
	}


	@Override
	@Transactional
	public void updateStock(Stock p) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Stock updated successfully!");
		
	}


	@Override
	@Transactional
	public List<Stock> listStocks() {
		Session session = this.sessionFactory.getCurrentSession();
        List<Stock> stockList = session.createQuery("from Stock").list();
        for(Stock p : stockList){
            logger.info("Person List::"+p);
        }
        return stockList;
	}


	@Override
	@Transactional
	public Stock getStockById(int id) {
		Session session = this.sessionFactory.getCurrentSession();      
        Stock p = (Stock) session.load(Stock.class, new Integer(id));
        logger.info("Person loaded successfully, Person details="+p);
        return p;
	}


	@Override
	@Transactional
	public void removeStock(int id) {
		Session session = this.sessionFactory.getCurrentSession();
        Stock p = (Stock) session.load(Stock.class, new Integer(id));
        if(null != p){
            session.delete(p);
        }
        logger.info("Person deleted successfully, person details="+p);
		
	}


	

	
}
