package com.aquivera.nn.dao;

import java.util.List;
import com.aquivera.nn.model.Contact;


public interface ContactDao {

	public void addContact(Contact contact);
	public void updateContact(Contact contact);
    public List<Contact> listContacts();
    public Contact getContactById(int contactId);
    public void removeContact(int contactId);
    
	
}
