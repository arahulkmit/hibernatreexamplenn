package com.aquivera.nn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aquivera.nn.model.Contact;
import com.aquivera.nn.model.MailTemplate;
import com.aquivera.nn.service.ContactService;
import com.aquivera.nn.service.MailTemplateService;
@Controller
public class TemplateController {	
private MailTemplateService templateService; 
	@Autowired(required = true)
	@Qualifier(value = "templateService")
	public void setTemplateService(MailTemplateService templateService) {
		this.templateService = templateService;
	}

//	@RequestMapping(value = "/contact/add", method = RequestMethod.GET)
//	public String addContact(@ModelAttribute("contact") Contact contact) {
//
//		this.contactService.addContact(contact);
//
//		return "redirect:/contacts";
//
//	}
	
	 @RequestMapping(value = "/templates", method = RequestMethod.GET)
	    public String listtemplates(Model model) {
	        model.addAttribute("template", new MailTemplate());
	        model.addAttribute("listTemplates", this.templateService.listMailTemplate());
	        return "mail";
	    }
	     
	    //For add and update person both
	    @RequestMapping(value= "/template/add", method = RequestMethod.POST)
	    public String addContact(@ModelAttribute("template") MailTemplate template){
	    	
	       if(template!=null && template.getMailTemplateID() == null){
	            //new person, add it
	            this.templateService.addMailTemplate(template);
	        }else{
	            //existing person, call update
	            this.templateService.updateMailTemplate(template);
	        }
	         
	        return "redirect:/templates";
	         
	    }	     
	    @RequestMapping("/removeTemplate/{id}")
	    public String removeStock(@PathVariable("id") int templateId){
	         
	        this.templateService.removeMailTemplate(templateId);
	        return "redirect:/templates";
	    }	  
	    @RequestMapping("/editTemplate/{id}")
	    public String editStock(@PathVariable("id") int templateId, Model model){
	        model.addAttribute("template", this.templateService.getMailTemplateById(templateId));
	        model.addAttribute("listTemplates", this.templateService.listMailTemplate());
	        return "mail";
	    }
}
