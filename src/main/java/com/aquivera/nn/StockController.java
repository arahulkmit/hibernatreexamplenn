package com.aquivera.nn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aquivera.nn.model.Stock;
import com.aquivera.nn.service.StockService;

@Controller
public class StockController {

	private StockService stockService; 

	@Autowired(required = true)
	@Qualifier(value = "stockService")
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	@RequestMapping(value = "/person/add", method = RequestMethod.GET)
	public String addStock(@ModelAttribute("stock") Stock s) {

		this.stockService.addStock(s);

		return "redirect:/persons";

	}
	
	 @RequestMapping(value = "/persons", method = RequestMethod.GET)
	    public String listStocks(Model model) {
	        model.addAttribute("stock", new Stock());
	        model.addAttribute("listStocks", this.stockService.listStocks());
	        return "person";
	    }
	     
	    //For add and update person both
	    @RequestMapping(value= "/person/add", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("stock") Stock p){
	    	System.out.println(p);
	    	
	       if(p!=null && p.getStockId() == null){
	            //new person, add it
	            this.stockService.addStock(p);
	        }else{
	            //existing person, call update
	            this.stockService.updateStock(p);
	        }
	         
	        return "redirect:/persons";
	         
	    }
	     
	    @RequestMapping("/remove/{id}")
	    public String removeStock(@PathVariable("id") int id){
	         
	        this.stockService.removeStock(id);
	        return "redirect:/persons";
	    }
	  
	    @RequestMapping("/edit/{id}")
	    public String editStock(@PathVariable("id") int id, Model model){
	        model.addAttribute("stock", this.stockService.getStockById(id));
	        model.addAttribute("listStocks", this.stockService.listStocks());
	        return "person";
	    }

}