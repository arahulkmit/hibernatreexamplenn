package com.aquivera.nn.service;

import java.util.List;


import com.aquivera.nn.model.MailTemplate;

public interface MailTemplateService {
	
	public void addMailTemplate(MailTemplate template);
	public void updateMailTemplate(MailTemplate template);
    public List<MailTemplate> listMailTemplate();
    public MailTemplate getMailTemplateById(int MailTemplateID);
    public void removeMailTemplate(int MailTemplateID);

}
