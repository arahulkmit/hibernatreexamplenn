package com.aquivera.nn.service;

import java.util.List;

import com.aquivera.nn.model.Event;;

public interface EventService {

	public void addEvent(Event s);
	public void updateEvent(Event p);
    public List<Event> listEvents();
    public Event getEventById(int id); 
    public void removeEvent(int id);
}
