package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.NotesDao;
import com.aquivera.nn.model.Notes;
import com.aquivera.nn.service.NotesService;

public class NotesServiceImpl implements NotesService{
	
	NotesDao NotesDao;	
	
	public void setNotesDao(NotesDao NotesDao) {
		this.NotesDao = NotesDao;
	}


	@Override
	public void addNotes(Notes s) {
		// TODO Auto-generated method stub
		this.NotesDao.addNotes(s);
		
	}


	@Override
	public void updateNotes(Notes p) {
		// TODO Auto-generated method stub
		this.NotesDao.updateNotes(p);
	}


	@Override
	public List<Notes> listNotes() {
		// TODO Auto-generated method stub
		return this.NotesDao.listNotes();
	}


	@Override
	public Notes getNotesById(int id) {
		// TODO Auto-generated method stub
		return this.NotesDao.getNotesById(id);
	}


	@Override
	public void removeNotes(int id) {
		// TODO Auto-generated method stub
		
		this.NotesDao.removeNotes(id);
		
	}

	
}
