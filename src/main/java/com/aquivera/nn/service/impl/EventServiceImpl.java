package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.EventDao;
import com.aquivera.nn.model.Event;
import com.aquivera.nn.service.EventService;

public class EventServiceImpl implements EventService{

	
	EventDao EventDao;
	
	
	public void setEventDao(EventDao EventDao) {
		this.EventDao = EventDao;
	}


	@Override
	public void addEvent(Event s) {
		// TODO Auto-generated method stub
		this.EventDao.addEvent(s);
		
	}


	@Override
	public void updateEvent(Event p) {
		// TODO Auto-generated method stub
		this.EventDao.updateEvent(p);
	}


	@Override
	public List<Event> listEvents() {
		// TODO Auto-generated method stub
		return this.EventDao.listEvents();
	}


	@Override
	public Event getEventById(int id) {
		// TODO Auto-generated method stub
		return this.EventDao.getEventById(id);
	}


	@Override
	public void removeEvent(int id) {
		// TODO Auto-generated method stub
		
		this.EventDao.removeEvent(id);
		
	}

	
}
