package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.ContactDao;
import com.aquivera.nn.model.Contact;
import com.aquivera.nn.service.ContactService;

public class ContactServiceImpl implements ContactService {

	ContactDao contactDao;
	
	public void setContactDao(ContactDao contactDao) {
		this.contactDao = contactDao;
	}

	@Override
	public void addContact(Contact contact) {
		// TODO Auto-generated method stub
		this.contactDao.addContact(contact);

	}

	@Override
	public void updateContact(Contact contact) {
		// TODO Auto-generated method stub
		this.contactDao.updateContact(contact);

	}

	@Override
	public List<Contact> listContacts() {
		// TODO Auto-generated method stub
		return this.contactDao.listContacts();
	}

	@Override
	public Contact getContactById(int contactId) {
		// TODO Auto-generated method stub
		return this.contactDao.getContactById(contactId);
	}

	@Override
	public void removeContact(int contactId) {
		// TODO Auto-generated method stub
		this.contactDao.removeContact(contactId);

	}

}
