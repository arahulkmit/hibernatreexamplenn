package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.MailTemplateDao;
import com.aquivera.nn.model.MailTemplate;
import com.aquivera.nn.service.MailTemplateService;

public class MailTemplateServiceImpl implements MailTemplateService {
	
	MailTemplateDao mailTemplateDao;

	
	
	public void setMailTemplateDao(MailTemplateDao mailTemplateDao) {
		this.mailTemplateDao = mailTemplateDao;
	}

	@Override
	public void addMailTemplate(MailTemplate template) {
		// TODO Auto-generated method stub
		this.mailTemplateDao.addMailTemplate(template);
		
	}

	@Override
	public void updateMailTemplate(MailTemplate template) {
		// TODO Auto-generated method stub
		this.mailTemplateDao.updateMailTemplate(template);
		
	}

	@Override
	public List<MailTemplate> listMailTemplate() {
		// TODO Auto-generated method stub
		return this.mailTemplateDao.listMailTemplate();
	}

	@Override
	public MailTemplate getMailTemplateById(int MailTemplateID) {
		// TODO Auto-generated method stub
		return this.mailTemplateDao.getMailTemplateById(MailTemplateID);
	}

	@Override
	public void removeMailTemplate(int MailTemplateID) {
		// TODO Auto-generated method stub
		this.mailTemplateDao.removeMailTemplate(MailTemplateID);
		
	}
	
	
}
