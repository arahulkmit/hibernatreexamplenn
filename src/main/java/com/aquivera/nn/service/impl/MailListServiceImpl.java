package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.ContactDao;
import com.aquivera.nn.dao.MailListDao;
import com.aquivera.nn.model.MailList;
import com.aquivera.nn.service.MailListService;

public class MailListServiceImpl implements MailListService {

	MailListDao mailListDao;
	
	public void setMailListDao(MailListDao mailListDao) {
		this.mailListDao = mailListDao;
	}
	@Override
	public void addMailList(MailList mailList) {
		// TODO Auto-generated method stub
		this.mailListDao.addMailList(mailList);
	}

	@Override
	public void updateMailList(MailList mailList) {
		// TODO Auto-generated method stub 
		this.mailListDao.updateMailList(mailList);
	}

	@Override
	public List<MailList> listMailList() {
		// TODO Auto-generated method stub
		return this.mailListDao.listMailList();
	}

	@Override
	public MailList getMailListById(int mailListID) {
		// TODO Auto-generated method stub
		return this.mailListDao.getMailListById(mailListID);
	}

	@Override
	public void removeMailList(int mailListID) {
		// TODO Auto-generated method stub
		this.mailListDao.removeMailList(mailListID);
	}

}
