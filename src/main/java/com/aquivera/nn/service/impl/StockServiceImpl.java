package com.aquivera.nn.service.impl;

import java.util.List;

import com.aquivera.nn.dao.StockDao;
import com.aquivera.nn.model.Stock;
import com.aquivera.nn.service.StockService;

public class StockServiceImpl implements StockService{

	
	StockDao stockDao;
	
	
	public void setStockDao(StockDao stockDao) {
		this.stockDao = stockDao;
	}


	@Override
	public void addStock(Stock s) {
		// TODO Auto-generated method stub
		this.stockDao.addStock(s);
		
	}


	@Override
	public void updateStock(Stock p) {
		// TODO Auto-generated method stub
		this.stockDao.updateStock(p);
	}


	@Override
	public List<Stock> listStocks() {
		// TODO Auto-generated method stub
		return this.stockDao.listStocks();
	}


	@Override
	public Stock getStockById(int id) {
		// TODO Auto-generated method stub
		return this.stockDao.getStockById(id);
	}


	@Override
	public void removeStock(int id) {
		// TODO Auto-generated method stub
		
		this.stockDao.removeStock(id);
		
	}

	
}
