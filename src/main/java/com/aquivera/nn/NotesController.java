package com.aquivera.nn;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aquivera.nn.model.Notes;
import com.aquivera.nn.service.NotesService;;

@Controller
public class NotesController {
	
	public NotesService notesService;
	
	
	@Autowired(required=true)
	@Qualifier(value="notesService")
	public void setNotesService(NotesService notesService){
		this.notesService = notesService;
	}
	
//	@RequestMapping(value = "/event/add", method = RequestMethod.GET)
//	public String addEvent(@ModelAttribute("event") Event s) {
//		this.eventService.addEvent(s);
//
//		return "redirect:/events";
//	}
	
	@RequestMapping(value = "/notes", method = RequestMethod.GET)
    public String listNotes(Model model) {
		System.out.println("heeeeeeey");
        model.addAttribute("note", new Notes());
        model.addAttribute("listNotes", this.notesService.listNotes());
        return "note";
        
    }
	
	//For add and update person both
    @RequestMapping(value= "/note/add", method = RequestMethod.POST)
    public String addNotes1(@Valid @ModelAttribute("note") Notes p){
    	System.out.println(p);
    	
       if(p!=null && p.getNotesId() == null){
            //new event, add it
            this.notesService.addNotes(p);
        }else{
            //existing event, call update
            this.notesService.updateNotes(p);
        }
       System.out.println("heoooooooooy"); 
        return "redirect:/notes";
         
    }
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
    
    @RequestMapping(value= "/note/data", method = RequestMethod.POST)
    public String addNotes2(@Valid @ModelAttribute("note") Notes p){
    	System.out.println(p);
    	System.out.println("heeeeeeey");
    	
       if(p!=null && p.getNotesId() == null){
            //new event, add it
            this.notesService.addNotes(p);
        }else{
            //existing event, call update
            this.notesService.updateNotes(p);
        }
         
        return "redirect:/notes";
         
    }
    
    @RequestMapping("/removeNotes/{id}")
    public String removeNotes(@PathVariable("id") int notesid){         
        this.notesService.removeNotes(notesid);
        return "redirect:/notes";
    }
	
    @RequestMapping("/editNotes/{id}")
    public String editNotes(@PathVariable("id") int eventid, Model model){ 
        model.addAttribute("note", this.notesService.getNotesById(eventid));
        model.addAttribute("listNotes" + "", this.notesService.listNotes());
        return "note";
    }
	
}
